/*
 Allows a user to access data in EEPROM using the serial 
 monitor. In the serial monitor the user should be able 
 to type one of two commands:  “read” and “write. "Read" 
 takes one argument, an EEPROM  address. "Write" takes 
 two arguments, an EEPROM address and a value.

 For example, if the user types “read 3” then the contents 
 of EEPROM address 3 should be printed to the serial monitor. 
 If the user types “write 3 10” then the value 10 should be 
 written into address 3 of the EEPROM.

 In order to grade this assignment, you will perform a 
 simulation on the website 123d.circuits.io. You will 
 need to create an account for free. There are instructional 
 videos on that website that will teach you how to use the 
 simulator. 
*/

#include <EEPROM.h>

char delim[] = " ,._;:";                       // command string parsing delimiters

void setup() {
  Serial.begin(9600);                          // start up the serial monitor
}

void loop() {
    
  String sstr;                                 // string from the serial monitor
  char *cstr;                                  // char string from the serial monitor 
  char *cmnd;                                  // parsed command
  char *temp;                                  // parsed temp character string 
  int  addr;                                   // address value [0 thru 1023]
  byte valu;                                   // memory value [0 thru 255] put
  byte mmry;                                   // memory value [0 thru 255] get

  int inp_avail = Serial.available();          // get serial monitor input amount
  
  if (inp_avail > 0)                           // ensure input available from the serial monitor
  {
    sstr = Serial.readString();                // input the initial string commend
    cstr = &sstr[0];                           // assign string to a character array
    Serial.print("Input  [serial]: ");         // output the input command string
    Serial.println(cstr);                      // back to the serial monitor 
    cmnd = strtok(cstr,delim);                 // parse command string token
    for(int i = 0; cmnd[i]; i++)               // convert every character in the 
    { cmnd[i] = tolower(cmnd[i]); }            // command string to lowercase
    if (strcmp(cmnd,"read") == 0)              // ensure read command processing
    {
      Serial.print("Input  [EEPROM}: read ");  // output EEPROM message to serial monitor
      temp = strtok(NULL,delim);               // parse write's address value
      if (isNumber(temp))                      // ensure write's address is a number
      {
        addr = atoi(temp);                     // sddress string to an integer
        if (addr < 1024)                       // ensure address range 1..1023
        {
          Serial.print(addr, DEC);             // output address to serial monitor
          mmry = EEPROM.read(addr);            // retrieve value from EEPROM memory
          Serial.print(" :=> ");               // output :=> to serial monitor
          Serial.print(mmry, DEC);             // output memory value to serial monitor
          Serial.print(" [0x");                // output hex indicator to serial monitor
          Serial.print(mmry, HEX);             // output hex memory value to serial monitor
          Serial.println("].");                // output closure to serial monitor
        }
        else
          Serial.println("err - read: addr out of range");
      }
      else
        Serial.println("err - read addr - not numeric");
    }
    else if (strcmp(cmnd,"write") == 0)        // ensure write command processing
    {
      Serial.print("Output [EEPROM]: write "); // output EEPROM message to serial monitor
      temp = strtok(NULL,delim);               // parse write's address value
      if (isNumber(temp))                      // ensure write's address is a number
      {
        addr = atoi(temp);                     // sddress string to an integer
        if (addr < 1024)                       // ensure address range 1..1023
        {
          Serial.print(addr, DEC);             // output address to serial monitor
          temp = strtok(NULL,delim);           // parse write's storage value
          if (isNumber(temp))                  // ensure the value is a number
          {
            if (atoi(temp) < 256)              // ensure value in range 0..255
            {
              valu = (byte) atoi(temp);        // convert value to a storage byte
              EEPROM.write(addr, valu);        // output value to EEPROM memory
              Serial.print(" :=> ");           // output :=> to serial monitor
              Serial.print(valu, DEC);         // output value to serial monitor
              Serial.print(" [0x");            // output hex indicator to serial monitor
              Serial.print(valu, HEX);         // output hex value to serial monitor
              Serial.println("].");            // output closure to serial monitor
            }
            else
              Serial.println(" err - write: valu out of range");
          }
          else
            Serial.println("err - write: valu not numeric");
        }
        else
          Serial.println("err - write: addr out of range");
      }
      else
        Serial.println("err - write: addr not numeric");
    }
    else
      Serial.println("EEPROM cmnd not 'read' or 'write.");
  }
/*else
  {
    // prompt every 5000 milliseconds [5 seconds]
    delay(5000);
    Serial.println("Awaiting Command ........ \n");
  }*/
}

// check if a character string is numeric
bool isNumber(char *chrstr) 
{
    size_t chrstrln = strlen(chrstr)-1;        // get string length
    for (int i=0; i<chrstrln; i++)             // for every character in string
      if (!isdigit(chrstr[i]))                 // ensure character is a digit
        return false;                          // if not, return false
    return true;                               // if so, return true
}


/*
  Developed, compiled and run via Arduino IDE by
  Reginald K. McDonald

  The Arduino Platfom and C Programming  [Nov, 2016]

  Assignment-4: 

  Instructions:

  Write a program that allows the user to control the LED
  connected to pin 13 of the Arduino. When the program is 
  started, the LED should be off. The user should open the 
  serial monitor to communicate with the Arduino.  If the 
  user sends the character '1' through the serial monitor 
  then the LED should turn on.  If the user sends the 
  character '0' through the serial monitor then the LED 
  should turn off ...
*/

const int LED = 13;
int switchLED;

void setup() {
     Serial.begin(9600);             // establish serial communication
     pinMode(LED, OUTPUT);           // set digital pin 13 to output mode
     digitalWrite(LED, LOW);         // ensure the Arduino LED is turned off
}

void loop() {
     if (Serial.available() > 0)            // if there's data in the input buffer
     {                                               // do the following:
        switchLED = Serial.read();     //    read byte/character
        if (switchLED == 0x30)          //    if it's the character '0' [ASCII]
           digitalWrite(LED, LOW);     //       turn off the Arduino LED
        else if (switchLED == 0x31)   //    if it's the character '1' [ASCII]
           digitalWrite(LED, HIGH);     //       turn on the Arduino LED
     }
}

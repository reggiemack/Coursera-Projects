/*
  Developed, compiled and run via Arduino IDE by
  Reginald K. McDonald

  The Arduino Platform and C Programming  [Nov, 2016]

  Assignment-3: 

  Instructions:

  Write a program that causes the built-in LED connected to pin 13 
  on the Arduino to blink, alternating between fast blinks and slow 
  blinks. The LED should blink 5 times, once every half second, and 
  then it should blink 5 more times, once every two seconds. The LED 
  should continue to blink in this alternating fashion for as long as
  the Arduino receives power.
*/

int i;

void setup() {
    i = 0;
    pinMode(13,OUTPUT);
}

void loop() {
    if (i < 5)                         // check for half second strobe
       {i++; strobe(500);}      // strobe LED by the half second
    else if (i < 10)                 // check for 2 second strobe
       {i++; strobe(2000);}     // strobe LED every 2 seconds
    else i = 0;                       // reset counter
}

void strobe (int freq) {           // freq is the number of milliseconds at which to strobe
       digitalWrite(13, HIGH);  // turn the LED on (HIGH is the voltage level)
       delay(freq);                    // wait for milliseconds contained in freq
       digitalWrite(13, LOW);   // turn the LED off (LOW is the voltage level)
       delay(freq);                    // wait for milliseconds contained in freq
}

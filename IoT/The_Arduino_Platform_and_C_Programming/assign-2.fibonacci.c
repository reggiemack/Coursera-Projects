#include <stdio.h>

/*
  Fibonacci.c
  Developed, compiled and run via Dev-C++ by
  Reginald K. McDonald

  The Arduino Platfom and C Programming  [Nov, 2016]
  
  Assignment-2: 

  Write a program in C that computes and prints out the
  first six digits in the Fibonacci sequence. Look up
  the definition of the Fibonacci sequence if you don't
  know it. The first two numbers in the sequence are 0
  and 1, but your program should compute the next four
  digits.
*/

void fibonacci(int num)
{
	int num1 = 0;
	int num2 = 1;
	printf("0, 1");

	int i;
	for (i=1; i<num-1; i++)
	{
		int sum = num1 + num2;
		printf(", %i", sum);
		num1 = num2;
		num2 = sum;
	}
}

void main()
{
	int num = 6;
	printf("The Fibonacci sequence of length %i \nis :===> ", num);
	fibonacci(num);
    printf(";\n");
}
